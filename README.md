# fileshare2000

## Lancement avec Docker

```bash
docker-compose up
```

## Installation manuelle

```bash
python -m venv venv
pip install -r requirements.txt
cd src
python app.py
```
