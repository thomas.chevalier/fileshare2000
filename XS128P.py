import z3
from random import randrange

class XS128p:
    u64_mask = (1<<64)-1
    exponent_shift = 52+1
    
    def __init__(self, a:int = randrange(1<<64), b:int = randrange(1<<64)):
        self.state = [a,b]

    def next(self):
        s1 = self.state[0]
        s0 = self.state[1]
        s1 ^= (s1<<23) & self.u64_mask
        
        self.state[0] = s0
        self.state[1] = s1 ^ s0 ^ (s1>>17) ^ (s0>>26)
        
        out = (self.state[1] + s0) & self.u64_mask
        return out

    def next_double(self):
        mantissa = self.next() & ((1<<self.exponent_shift)-1)
        return float(mantissa) / (1<<self.exponent_shift)
    
    def next_char(self):
        return int(256*self.next_double())

    def next_int(self):
        return int((1<<32)*self.next_double())

    def rewind_state(self):
        solver = z3.Solver()
        x,y = z3.BitVec('x',64),z3.BitVec('y',64)

        s1 = x
        s0 = y
        s1 ^= s1<<23

        solver.add(s0 == self.state[0])
        solver.add(s1 ^ s0 ^ z3.LShR(s1,17) ^ z3.LShR(s0,26) == self.state[1])

        assert(solver.check() == z3.sat)
        model = solver.model()
        self.state[0] = model[x].as_long()
        self.state[1] = model[y].as_long()
        solver.add(z3.Or(x != self.state[0], y != self.state[1]))
        assert(solver.check() == z3.unsat)

    def get_seed(L, mask=(1<<64)-1, shift=0):
        solver = z3.Solver()
        x,y = z3.BitVec('x',64),z3.BitVec('y',64)

        state = [x,y]
        for l in L:
            s1 = state[0]
            s0 = state[1]
            s1 ^= s1<<23
            
            state[0] = s0
            state[1] = s1 ^ s0 ^ z3.LShR(s1,17) ^ z3.LShR(s0,26)
            
            out = state[1] + s0
            out = z3.LShR(out,shift) & mask
            solver.add(out == l)

        while solver.check() == z3.sat:
            model = solver.model()
            xx = model[x].as_long()
            yy = model[y].as_long()

            solver.add(z3.Or(x != xx, y != yy))
            yield xx,yy

    def get_seed_float(L):
        L = [int(l * (1<<XS128p.exponent_shift)) for l in L]
        return XS128p.get_seed(L, mask=(1<<XS128p.exponent_shift)-1)

    def get_seed_char(L):
        return XS128p.get_seed(L, mask=0xFF, shift=XS128p.exponent_shift-8)

    def get_seed_int(L):
        return XS128p.get_seed(L, mask=(1<<32)-1, shift=XS128p.exponent_shift-32)