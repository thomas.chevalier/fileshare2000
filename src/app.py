from curses.ascii import DEL
from flask import Flask, request, render_template, redirect, abort, send_file
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import text
import hashlib
import uuid
import os

app = Flask(__name__)
CORS(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite3'
app.config['SECRET_KEY'] = 'a random string'
db = SQLAlchemy(app)

CURRENT_PATH = "/".join(os.path.realpath(__file__).split("/")[:-1])
DOWNLOAD_PATH = "api/download"
DELETE_PATH = "api/delete"
NEW_PATH = "api/new"
MONITOR_PATH = "monitor"
STORE_PATH = "uploaded-file"

class Record(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    file_hash = db.Column(db.String(32))
    file_path = db.Column(db.String(100))
    file_del = db.Column(db.String(6))
    nb_download = db.Column(db.Integer)
    def __init__(self, file_hash, uid, file_del):
        self.file_hash = file_hash
        self.file_path = "uploaded-files/" + uid
        self.file_del = file_del
        self.nb_download = 0

# # POUR LA PREPROD
# @app.route('/show_db')
# def show_db():
#     return render_template("show.html", records=Record.query.all())

# # POUR LA PREPROD
# @app.route('/clear_db')
# def clear_db():
#     db.drop_all()
#     db.create_all()
#     return "DB cleared"

@app.route('/')
def index():
    return render_template('home.html')

@app.route('/%s'%NEW_PATH, methods = ['POST'])
def upload():
    '''
    This route is to upload a ciphered file
    '''
    file_data = request.files['data'].read()
    file_hash = hashlib.md5(file_data).hexdigest()
    file_uid = uuid.uuid4().hex
    file_del = uuid.uuid4().hex[:6]

    with open('%s/uploaded-files/%s'%(CURRENT_PATH, file_uid), 'wb') as f:
        f.write(file_data)
        f.close()
    
    r = Record(file_hash, file_uid, file_del)
    db.session.add(r)
    db.session.commit()

    return {"download_token": file_uid, "private_token": file_del}

@app.route('/summary/<string:uid>')
def summary(uid):
    '''
    Show a summary of how to access the file
    '''
    return render_template('summary.html', download_token=uid)

@app.route('/download/<string:uid>')
def user_download(uid):
    return render_template('download.html', download_token=uid)

@app.route('/%s/<string:uid>'%DOWNLOAD_PATH)
def download(uid):
    '''
    This route is to download a cipher file from its UID, shared by the owner
    '''
    r = Record.query.filter_by(file_path='uploaded-files/%s'%uid).first()
    if r is None:
        abort(404)
    else:
        r.nb_download += 1
        db.session.commit()
        return send_file("%s/uploaded-files/%s"%(CURRENT_PATH, uid), as_attachment=True)

@app.route('/%s/<string:token>'%DELETE_PATH)
def delete(token):
    '''
    This route is to download a cipher file from its del_token, only known by the owner
    '''
    r = Record.query.filter_by(file_del=token).first()
    if r is None:
        abort(404)
    else :
        related_uid = r.file_path.split("/")[-1]
        os.remove("%s/uploaded-files/%s"%(CURRENT_PATH, related_uid))
        db.session.delete(r)
        db.session.commit()
        return render_template('delete.html', download_token=related_uid)

@app.route('/%s/<string:del_token>'%MONITOR_PATH)
def monitor(del_token) :
    '''
    This route is, for the owner of a file, to check how many times a file uploaded has been downloaded.
    Credit to our undergrad intern for this small feature.
    '''
    request = text("SELECT file_path, nb_download FROM Record WHERE file_del='%s'"%del_token)
    result = db.session.execute(request)
    rows = result.fetchall()
    if len(rows) == 0:
        abort(404)
    else:
        uid = rows[0].file_path.split("/")[-1]
        return render_template('monitor.html', download_token=uid, nb_download=rows[0].nb_download)

@app.errorhandler(404)
def not_found(e):
    return render_template('404.html')

if __name__ == '__main__':
    with app.app_context():
        db.drop_all()
        db.create_all()

    app.run(host='0.0.0.0')
