function get_rand_128() {
    let x = new Uint32Array(4);
    for (let i = 0; i < 4; i++) {
        x[i] = (2 ** 32) * Math.random();
    }
    return x.buffer;
}

function buf2hex(buffer) { // buffer is an ArrayBuffer
    return [...new Uint8Array(buffer)]
        .map(x => x.toString(16).padStart(2, '0'))
        .join('');
}

function hex2buf(string) {
    let bytes = [];
    string.replace(/../g, function (pair) {
        bytes.push(parseInt(pair, 16));
    });
    return new Uint8Array(bytes);
}

async function cipher_data(filename, buffer) {
    const data = new Uint8Array(buffer);

    // Random generation of key and IV
    const keyData = get_rand_128();
    const key = await crypto.subtle.importKey(
        "raw",
        keyData,
        "AES-CBC",
        false,
        ["encrypt"]
    );
    const iv = new Uint8Array(get_rand_128());

    // Compute metadata
    const hash = (await window.crypto.subtle.digest("SHA-256", data))
    const encoder = new TextEncoder();
    const metadata = encoder.encode(JSON.stringify({ filename, hash: buf2hex(hash) }));

    // Append metadata to the file
    const bundledDataBuffer = new ArrayBuffer(metadata.byteLength + data.byteLength);
    const bundledData = new Uint8Array(bundledDataBuffer);
    bundledData.set(metadata, 0);
    bundledData.set(data, metadata.byteLength);

    // Cipher file and metadata
    const ciphered = new Uint8Array(
        await window.crypto.subtle.encrypt({ name: "AES-CBC", iv: iv }, key, bundledData)
    );

    // Construct the binary blob
    const blobBuffer = new ArrayBuffer(16 + ciphered.byteLength);
    const blob = new Uint8Array(blobBuffer);
    blob.set(iv, 0);
    blob.set(ciphered, 16);

    return { key: buf2hex(keyData), blob: new Blob([blob]) };
}


async function decrypt_data(buffer, hexKey) {
    const data = new Uint8Array(buffer);
    const iv = data.slice(0, 16);
    const keyData = hex2buf(hexKey);

    console.log(keyData)
    const key = await crypto.subtle.importKey(
        "raw",
        keyData.buffer,
        "AES-CBC",
        false,
        ["decrypt"]
    );

    const plain = new Uint8Array(
        await window.crypto.subtle.decrypt({ name: "AES-CBC", iv: iv }, key, data.slice(16))
    );

    const metadataIndex = plain.indexOf('}'.charCodeAt(0));

    const decoder = new TextDecoder();
    const metadata = JSON.parse(decoder.decode(plain.slice(0, metadataIndex + 1)))
    console.log(metadata)

    return { metadata, blob: new Blob([plain.slice(metadataIndex + 1)]) }
}