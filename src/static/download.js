function saveFile(blob, filename) {
    if (window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(blob, filename);
    } else {
        const a = document.createElement('a');
        document.body.appendChild(a);
        const url = window.URL.createObjectURL(blob, { type: 'application/octet-stream' });
        a.href = url;
        a.download = filename;
        a.click();
        setTimeout(() => {
            window.URL.revokeObjectURL(url);
            document.body.removeChild(a);
        }, 0)
    }
}


async function onFileDownload() {
    console.log("On file download")
    const paths = window.location.pathname.split('/');
    const download_token = paths[paths.length - 1];

    const key = window.location.hash.slice(1);

    const blob = await axios({
        url: `/api/download/${download_token}`, method: 'GET', responseType: 'blob'
    });

    const reader = new FileReader();
    reader.onload = async e => {
        data = e.target.result;
        const { metadata, blob } = await decrypt_data(data, key);

        saveFile(blob, metadata.filename);
    }
    reader.readAsArrayBuffer(blob.data);

}