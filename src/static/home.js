async function uploadFile(blob) {
    const formData = new FormData();
    formData.append('data', blob, 'file.dat');

    console.log(formData)

    const res = await axios.post("/api/new", formData, {
        onUploadProgress: progressEvent => {
            const percentCompleted = Math.round(
                (progressEvent.loaded * 100) / progressEvent.total
            );
            const progressBar = document.getElementById("upload-progress");
            if (progressBar) {
                progressBar.setAttribute("aria-valuenow", percentCompleted);
                progressBar.setAttribute("style", `width: ${percentCompleted}%`);
            }
        },
        headers: {
            "Content-Type": "multipart/form-data"
        }
    });

    const downloadToken = res.data['download_token'];
    const privateToken = res.data['private_token'];

    return { downloadToken, privateToken }
}

function onFileUpload(element) {
    if (!element.files || !element.files[0]) {
        return;
    }
    const filename = element.files[0].name;

    const reader = new FileReader();
    reader.onload = async e => {
        data = e.target.result;
        const { key, blob } = await cipher_data(filename, data);

        const tokens = await uploadFile(blob);

        // Redirect to summary page
        window.location = `/summary/${tokens.downloadToken}?private=${tokens.privateToken}#${key}`;
    }
    reader.readAsArrayBuffer(element.files[0]);

}

