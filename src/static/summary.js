function get_full_url(path) {
    const host = `${window.location.protocol}//${window.location.host}`;
    return `${host}/${path}`
}

function create_link_append(parent, path) {
    const link = document.createElement('a');
    const href = get_full_url(path);
    link.setAttribute('href', href);
    link.innerHTML = href;
    parent.appendChild(link);
}

function initialize() {
    const paths = window.location.pathname.split('/');
    const download_token = paths[paths.length - 1];
    const key = window.location.hash.slice(1);

    const urlParams = new URLSearchParams(window.location.search);
    const private_token = urlParams.get('private');

    const downloadUrlElement = document.getElementById('download_url');
    create_link_append(downloadUrlElement, `download/${download_token}#${key}`)

    const monitorUrlElement = document.getElementById('monitor_url');
    if (private_token) {
        create_link_append(monitorUrlElement, `monitor/${private_token}`)
    } else {
        monitorUrlElement.innerHTML = "Indisponible"
    }

    const deleteUrlElement = document.getElementById('delete_url');
    if (private_token) {
        create_link_append(deleteUrlElement, `api/delete/${private_token}`)
    } else {
        deleteUrlElement.innerHTML = "Indisponible"
    }
}


document.addEventListener('DOMContentLoaded', (event) => initialize())